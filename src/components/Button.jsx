const Button = ({
  label,
  iconURL,
  backgroundColor,
  borderColor,
  textColor,
  fullWidth,
}) => {
  return (
    <button
      className={`flex justify-center items-center gap-2 px-7 py-4 border font-montserrat text-lg leading-none
      ${
        backgroundColor
          ? `${backgroundColor} ${textColor} ${borderColor}`
          : "bg-coral-red rounded-full text-white border-coral-red"
      } rounded-full ${fullWidth && "w-full"}`}>
      {label}
      {iconURL && (
        <img
          src={iconURL}
          alt='arrow right icon'
          className='h-5 w-5 rounded-full ml-2'
        />
      )}
    </button>
  );
};

export { Button };
