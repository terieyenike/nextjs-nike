import { star } from "../assets/icons";

const ReviewCard = ({ imgURL, customerName, rating, feedback }) => {
  return (
    <div className='flex flex-col justify-center items-center'>
      <img
        src={imgURL}
        alt={customerName}
        className='w-[120px] h-[120px] rounded-full object-cover'
      />
      <p className='mt-6 info-text text-center max-w-sm'>{feedback}</p>
      <div className='mt-3 flex justify-center items-center gap-2.5'>
        <img
          src={star}
          alt='star icon'
          width={24}
          height={24}
          className='object-contain m-0'
        />
        <p className='text-xl text-slate-gray font-montserrat'>{rating}</p>
      </div>
      <h3 className='font-bold text-3xl font-palanquin mt-1 text-center'>
        {customerName}
      </h3>
    </div>
  );
};

export { ReviewCard };
