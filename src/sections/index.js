import { CustomerReviews } from "./CustomerReviews";
import { Hero } from "./Hero";
import { Footer } from "./Footer";
import { SuperQuality } from "./SuperQuality";
import { PopularProducts } from "./PopularProducts";
import { Services } from "./Services";
import { SpecialOffer } from "./SpecialOffer";
import { Subscribe } from "./Subscribe";

export {
  CustomerReviews,
  Hero,
  Footer,
  SuperQuality,
  PopularProducts,
  Services,
  SpecialOffer,
  Subscribe,
};
