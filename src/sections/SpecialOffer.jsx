// import { arrowRight } from "../assets/icons";
import { offer } from "../assets/images";
import { Button } from "../components/Button";

const SpecialOffer = () => {
  return (
    <section className='flex justify-wrap items-center max-xl:flex-col-reverse gap-10 max-container'>
      <div className='flex-1'>
        <img
          src={offer}
          alt='offer'
          width={773}
          height={687}
          className='object-contain w-full'
        />
      </div>
      <div className='flex flex-col flex-1'>
        <h2 className='font-palanquin text-4xl capitalize font-bold lg:max-w-lg'>
          <span className='text-coral-red'>Special </span> Offer
        </h2>
        <p className='mt-4 lg:max-w-lg info-text'>
          Looking for unbeatable deals on your favorite products? Look no
          further! Our special offer is here to make your shopping experience
          even more exciting. Don&apos;t miss out on these amazing discounts,
          available for a limited time only.
        </p>
        <p className='mt-6 lg:max-w-lg info-text'>
          Start shopping now and make the most of this special offer! Hurry,
          before stocks run out!
        </p>
        <div className='mt-11 flex flex-wrap gap-4'>
          <Button label='View details' />
          <Button
            label='Learn more'
            backgroundColor='bg-white'
            borderColor='border-slate-gray'
            textColor='text-slate-gray'
          />
        </div>
      </div>
    </section>
  );
};

export { SpecialOffer };
